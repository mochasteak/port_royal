# Port Royal - as envisioned by Brian Bishop
# June 2015

import random, pygame, sys
from pygame.locals import *

# set up global variables
FPS = 30
WIDTH = 1024
HEIGHT = 768
CARDWIDTH = 100
CARDHEIGHT = 150
MARGIN = 10

#			R	G	B
GRAY	 = (100, 100, 100)
NAVYBLUE = ( 60,  60, 100)
WHITE	= (255, 255, 255)
RED	  = (255,   0,   0)
GREEN	= (  0, 255,   0)
BLUE	 = (  0,   0, 255)
YELLOW   = (255, 255,   0)
ORANGE   = (255, 128,   0)
PURPLE   = (255,   0, 255)
CYAN	 = (  0, 255, 255)
BLACK    = (0, 0, 0)

BGCOLOR = WHITE
LIGHTBGCOLOR = GRAY

cardBackImg = pygame.image.load('card-back.jpg')


def game():
	global FPSCLOCK, DISPLAYSURF
	pygame.init()
	FPSCLOCK = pygame.time.Clock()
	DISPLAYSURF = pygame.display.set_mode((WIDTH, HEIGHT))

	# set up variables
	mousex = 0 # used to store x coordinate of mouse event
	mousey = 0 # used to store y coordinate of mouse event
	num_players = 2
	boxx = None
	boxy = None

	pygame.display.set_caption('Port Royal, kind of...')

	DISPLAYSURF.fill(BGCOLOR)

	while True: # main game loop

		DISPLAYSURF.fill(BGCOLOR) # drawing the window

		# event handling loop
		for event in pygame.event.get(): 
			if event.type == QUIT or (event.type == KEYUP and event.key == K_ESCAPE):
				pygame.quit()
				sys.exit()
			elif event.type == MOUSEMOTION:
				mousex, mousey = event.pos
			elif event.type == MOUSEBUTTONUP:
				mousex, mousey = event.pos
				boxx, boxy = getBoxAtPixel(mousex, mousey) # remember to take this out when done
		
		askForPlayers(DISPLAYSURF)
		
		# Determine how many players in the game
		# boxx, boxy = getBoxAtPixel(mousex, mousey)
		if boxx != None and boxy != None:
			if boxx == 2 and boxy == 3:
				num_players = 2
			elif boxx == 3 and boxy == 3:
				num_players = 3
			elif boxx == 4 and boxy == 3:
				num_players = 4
			elif boxx == 5 and boxy == 3:
				num_players = 5
			elif boxx == 6 and boxy == 3:
				num_players = 6


		# Redraw the screen and wait a clock tick.
		pygame.display.update()
		FPSCLOCK.tick(FPS)

def askForPlayers(display):
	# Draw the text
	font = pygame.font.Font('freesansbold.ttf', 32)
	welcomeSurf = font.render("Welcome to Port Royal!", True, BLACK)
	welcomeRect = welcomeSurf.get_rect()
	welcomeRect.center = ((WIDTH / 2), (HEIGHT / 10))

	howManySurf = font.render("How many players in this game?", True, BLACK)
	howManyRect = howManySurf.get_rect()
	howManyRect.center =  ((WIDTH / 2), ((HEIGHT / 2) - 75))

	x = CARDWIDTH * 2 + MARGIN * 3
	offset = CARDWIDTH + MARGIN
	player = 2

	# Draw the four boxes to determine number of players
	for i in range(5):
		display.blit(cardBackImg, (pygame.draw.rect(DISPLAYSURF, NAVYBLUE, 
			(x, ((CARDHEIGHT + MARGIN) * 3), CARDWIDTH, CARDHEIGHT), 1)))
		playerSurf = font.render(str(player), True, WHITE)
		playerRect = playerSurf.get_rect()
		playerRect.center = (x + (CARDWIDTH /2), (((CARDHEIGHT + MARGIN) * 3) + (CARDHEIGHT / 4)))
		display.blit(playerSurf, playerRect)
		x = x + offset
		player += 1

	display.blit(welcomeSurf, welcomeRect)
	display.blit(howManySurf, howManyRect)
	

def leftTopCoordsOfBox(boxx, boxy):
	# Convert board coordinates to pixel coordinates
	left = boxx * (CARDWIDTH + MARGIN) + MARGIN
	top = boxy * (CARDHEIGHT + MARGIN)
	return (left, top)


def getBoxAtPixel(x, y):
	# Determine if mouse click happened over a box
	for boxx in range(WIDTH):
		for boxy in range(HEIGHT):
			left, top = leftTopCoordsOfBox(boxx, boxy)
			boxRect = pygame.Rect(left, top, CARDWIDTH, CARDHEIGHT)
			if boxRect.collidepoint(x, y):
				print("Clicked!")
				return (boxx, boxy)
	return (None, None)


if __name__ == '__main__':
    game()


