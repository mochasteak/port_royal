import random
from collections import Counter



class Card(object):
	"""a class to hold cards"""
	def __init__(self, type, cost=0, coins=0, color="", name=""):
		super(Card, self).__init__()
		self.type = type
		self.color = color
		self.cost = cost
		self.coins = coins
		self.name = ""

	def get_type(self):
		return self.type

	def get_color(self):
		return self.color

	def get_cost(self):
		return self.cost

	def get_coins(self):
		return self.coins


class Deck(object):
	"""a class to hold a deck"""
	def __init__(self):
		super(Deck, self).__init__()
		self.cards = []

	def len(self):
		return len(self.cards)

	def shuffle(self):
		random.shuffle(self.cards)

	# adds the type and number of cards specified to the deck
	def append_cards(self, type, number, cost=0, coins=0, color="blank", name="blank"):
		original_len = self.len()
		while self.len() < original_len + number:
			self.cards.append(Card(type, cost, coins, color, name))

	# adds all the correct cards to a deck to start a game
	def make_new_deck(self):
		print("BEFORE - The deck contains: " + str(len(self.cards)) + " cards")
		del self.cards[:]
		self.append_cards("ship", 10, coins=3, color="red")
		self.append_cards("ship", 10, coins=4, color="black")
		self.append_cards("ship", 10, coins=2, color="blue")
		self.append_cards("ship", 10, coins=3, color="yellow")
		self.append_cards("ship", 10, coins=4, color="green")
		self.append_cards("expedition", 5)
		self.append_cards("person", 15, cost=4, name="Sailor")
		self.append_cards("person", 15, cost=6, name="Admiral")
		self.append_cards("person", 15, cost=7, name="Governor")
		self.append_cards("person", 15, cost=9, name="Fraulein")
		self.append_cards("tax", 20)
		self.shuffle()
		print("AFTER - The deck now contains: " + str(len(self.cards)) + " cards")

	def pop(self):
		return self.cards.pop()

	def append(self, card):
		self.cards.append(card)

	def p(self):
		for card in self.cards:
			print(card.get_type() + " - Color: " + str(card.get_color()) + " - Coins: " + str(card.get_coins()) + " - Cost: " + str(card.get_cost()))
	
	#check to see if there are any two ships of same color on the board
	def num_ships(self):
		ships = []
		for card in self.cards:
			if card.get_type() == "ship":
				ships.append(card.get_color())
		
		print("There are " + str(len(ships)) + " ships on the board")
		
		ship_count = Counter(ships)

		if 2 in ship_count.values():
			return True
		else:
			return False


def deal(deck, board):
	dealt_card = deck.pop()
	board.append(dealt_card)
	print("Dealt card was " + dealt_card.get_type() + " Coins: (" + str(dealt_card.get_coins()) + ")  Cost: (" + str(dealt_card.get_cost()) + ") ")
	check_board_status(board)

def check_board_status(board):
	
	# display contents of the board
	print("Board contains: ")
	board.p()
	print("--------------")

	if len(board.cards) == 0:
		deal(game.deck, game.board)
	
	# check if there are too many ships...
	if board.num_ships():
		print("Oh. So sorry. There are two ships of the same color. Turn over.")
		print("--------------")
		game.end_turn()

	# ...or if card is expedition, if so, move it to expedition deck
	if board.cards[-1].get_type() == "expedition":
		print("We have a new expedition. Adding it to the expedition pile")
		game.expeditions.append(game.board.pop())
		print("Expeditions contains: " + str(len(game.expeditions.cards)))
		check_board_status(board)

	# ...or if card is a tax, tax players immediately and notify them
	if board.cards[-1].get_type() == "tax":
		print("We got a tax y'all!")
		for player in game.players:
			if player.get_coins() >= 12:
				cards_to_lose = int(player.get_coins() / 2)
				print("Player must lose " + str(cards_to_lose) + " cards.")
				while cards_to_lose > 0:
					game.discard.append(player.money.pop())
					cards_to_lose -= 1
		game.discard.append(game.board.pop())
		check_board_status(board)

	# if none of those... option to deal another card, buy, or end turn
	else:
		another_card = raw_input("Another card? (Y or N), Buy (B), or Quit (X)")

		if another_card.lower() == "y":	
			print("-----------------")
			deal(game.deck, game.board)
		
		elif another_card.lower() == "n": 
			print("-----------------")
			print("Round over.")
			game.end_turn()

		#  ======= Buying logic	========
		elif another_card.lower() == "b":
			print("-----------------")
			
			# First check if it's a ship. If it is, deal correct num of cards to player's money deck
			if game.board.cards[-1].get_type() == "ship":
				
				# Move the right num of cards from deck to player.money
				coins_to_give = game.board.cards[-1].get_coins()
				while coins_to_give > 0:
					game.players[game.active_position].money.append(game.deck.pop())
					coins_to_give -= 1

				# Move the ship to the discard pile
				game.discard.append(game.board.pop())
				print("-----------------")
				print(game.players[game.active_position].name + " now has " + str(game.players[game.active_position].get_coins()) + " coins.")
				game.end_turn()

			# Check if the player has enough coins to buy, if so move it to player's board
			elif game.players[game.active_position].get_coins() > 0 and game.players[game.active_position].get_coins() >= game.board.cards[-1].get_cost():
				bought_card = game.board.pop()
				game.players[game.active_position].pay(bought_card.get_cost())
				game.players[game.active_position].board.append(bought_card)
				print("-----------------")
				print("Congratulations, you just bought a " + game.players[game.active_position].board.cards[-1].get_type())
				print(game.players[game.active_position].name + " now has " + str(game.players[game.active_position].get_coins()) + " coins.")
				game.end_turn()

			# If they don't, tell them and give them the options again
			else:
				print("You don't have enough coins to buy this card")
				print("-----------------")
				check_board_status(board)

		elif another_card.lower() == "x":
			print("-----------------")
			print("Quitting game.")
		else:
			print("-----------------")
			print("What the hell did you type?")
			check_board_status(board)

def clear_board():
	while len(game.board.cards) > 0:
		cleared_card = game.board.pop()
		game.discard.append(cleared_card)
	print("We just cleared the board!")
	print(str(len(game.discard.cards)) + " cards now in the Discard pile")
	print(str(len(game.deck.cards)) + " cards now in the deck")
	print("-----------------")


class Player(object):
	def __init__(self, name):
		super(Player, self).__init__()
		self.name = name
		self.board = Deck()
		self.money = Deck()

	def name(self):
		return self.name

	def add_coins(self, amount):
		self.money += amount

	def pay(self, amount):
		paid_cards = 0
		while paid_cards < amount:
			game.discard.append(self.money.pop())
			paid_cards += 1
		print(self.name() + " now has " + self.get_coins() + " coins.")

	def get_coins(self):
		return len(self.money.cards)


class Game(object):
	def __init__(self):
		super(Game, self).__init__()
		self.players = []
		self.deck = Deck()
		self.board = Deck()
		self.discard = Deck()
		self.active_position = 0
		self.active_player = ""
		self.expeditions = Deck()


	def start_new_game(self):
		self.deck.make_new_deck()

		# Ask how many players there will be
		num_players = abs(int(raw_input("How many players in this game of Port Royal?: [Enter a number]")))

		# Handle invalid answers
		if num_players == 0:
			print("Has to be at least 1 player")
			num_players = int(raw_input("How many players in this game of Port Royal?: "))

		# Add as many new players as indicated
		while len(self.players) < num_players:
			self.players.append(Player(raw_input("Player {} enter your name: ".format(len(self.players) + 1))))

		print("-----------------")
		print("Let's get started!")

		# Make the first player the active player
		self.active_player = self.players[0].name
		print("Player " + self.players[0].name + " is the active player.")
		print("Player has " + str(self.players[0].get_coins()) + " coins.")

		print("-----------------")

	def increment_active_player(self):
		# If current position is already last, start again at 0
		if self.active_position == len(self.players) - 1:
			self.active_player = self.players[0].name
			self.active_position = 0
			print("Player " + self.players[0].name + " is the active player.")
		
		# If not, increment the position/player
		else:
			self.active_position += 1
			self.active_player = self.players[self.active_position].name
			print("The active player is now " + self.active_player)
	
	def end_turn(self):
		game.increment_active_player()
		clear_board()
		deal(game.deck, game.board)

	# Notice that I am repeating code a lot for identifying the active player. Make a method for this?
	#def active_player(self):
		#return self.players[self.active_position]


game = Game()
game.start_new_game()

deal(game.deck, game.board)



